﻿using BO.API.Services;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lemonway.Linq2Sql;
using log4net;
using System;
using System.Linq;
using System.Web.Http;

namespace BO.API.App_Start
{
    [Authorize]
    public class P2PController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(P2PController));

        // GET: api/P2P/id
        public LoadResult Get(int id, DataSourceLoadOptions opt)
        {
            try
            {
                var connectionString = Helper.GetConnectionString();
                var db = new MbDataContext(connectionString);
                db.Log = new Linq2SqlLogger();

                var dataSource = db.p2p_transactions.Where(x => x.moneyTransactionId == id);
                var res = DataSourceLoader.Load(dataSource, opt);
                return res;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}

﻿using BO.API.Services;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lemonway.Linq2Sql;
using Lemonway.Linq2Sql.Mb;
using log4net;
using System;
using System.Linq;
using System.Web.Http;

namespace BO.API.Controllers
{
    [Authorize]
    public class RelatedTransactionsController : ApiController
    {
        public class Source
        {
            public long? id;
            public string type;
            public DateTime? date;
            public byte? paymentMethod;
            public decimal? amount;
            public decimal? commission;
            public long? senderIntId;
            public string senderExtId;
            public long? receiverIntId;
            public string receiverExtId;
            public int status;
            public string comment;
            public bool? isChargebackRelated;
            public int? delayedDays;
            public long? rib;
            public string maskedCard;
            public string wkToken;
            public string merchantToken;
            public long? sddMandate;
            public DateTime? collectionDate;
            public DateTime? settlementDate;
            public DateTime? scheduledDate;
        }
        private static readonly ILog Log = LogManager.GetLogger(typeof(RelatedTransactionsController));
        // GET: api/Operations/[relate]
        public LoadResult Get([FromUri] int relatedId, DataSourceLoadOptions opt)
        {
            try
            {
                var connectionString = Helper.GetConnectionString();
                var db = new MbDataContext(connectionString);

                var res1 = db.money_transactions
                    .Where(tr => tr.parentTransactionId == relatedId)
                    .Select(x => new Source {
                        id = x.id,
                        type = x.isMoneyOut == 1 ? "out" : "in",
                        date = x.date,
                        paymentMethod = x.paymentMethod,
                        amount = x.amount,
                        commission = x.amountCom,
                        senderIntId = x.isMoneyOut == 1 ? x.walletIntId : (long?)null,
                        senderExtId = x.isMoneyOut == 1 ? x.wallet : null,
                        receiverIntId = x.isMoneyOut == 0 ? x.walletIntId : (long?)null,
                        receiverExtId = x.isMoneyOut == 0 ? x.wallet : null,
                        status = (int)x.status,
                        comment = x.comment,
                        isChargebackRelated = x.isChargebackRelated,
                        delayedDays = x.delayedDays,
                        rib = x.rib,
                        maskedCard = x.maskedCard,
                        wkToken = x.wkToken,
                        merchantToken = x.merchantToken,
                        sddMandate = x.sddMandate,
                        collectionDate = x.collectionDate,
                        settlementDate = x.settlementDate,
                        scheduledDate = null
                    });
                var res2 = db.p2p_transactions
                    .Where(p2p => p2p.moneyTransactionId == relatedId)
                    .Select(x => new Source {
                        id = x.id,
                        type = "p2p",
                        date = x.date,
                        paymentMethod = x.paymentMethod,
                        amount = x.amount,
                        commission = null,
                        senderIntId = x.walletSenderIntId,
                        senderExtId = x.walletSender,
                        receiverIntId = x.walletReceiverIntId,
                        receiverExtId = x.walletReceiver,
                        status = (int)x.status,
                        comment = x.message,
                        isChargebackRelated = null,
                        delayedDays = null,
                        rib = null,
                        maskedCard = null,
                        wkToken = null,
                        merchantToken = null,
                        sddMandate = null,
                        collectionDate = null,
                        settlementDate = null,
                        scheduledDate = x.scheduledDate
                    });

                var dataSource = res1.Union(res2);

                var res = DataSourceLoader.Load(dataSource, opt);
                return res;
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
        
    }
}

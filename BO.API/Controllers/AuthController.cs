﻿using BO.API.Services;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace BO.API.Controllers
{
    //[EnableCors(origins: "http://localhost:8080", headers: "*", methods: "*")]
    public class AuthController : ApiController
    {
        public class AuthResponse
        {
            public string token;
        }        
        public class PostParams
        {
            public string login;
            public string password;
        }
        //POST: api/Auth
        public AuthResponse Post([FromBody] PostParams p)
        {
            var connStr = Helper.GetConnectionString();
            var auth = new Authenticator(connStr);
            var returnStt = auth.CheckAuthority(p.login, p.password);

            if (returnStt != 0)
            {
                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.Unauthorized));
            }
                
            var secretKey = Encoding.ASCII.GetBytes("aVeryLongSecretKey");
            var signinCredentials = new SigningCredentials(new SymmetricSecurityKey(secretKey), SecurityAlgorithms.HmacSha256);

            var tokenContent = new JwtSecurityToken(
                audience: "LemonWay.bo",
                issuer: "issuer",
                claims: new List<Claim> {
                    new Claim("loginAs", "myLogin"),
                    new Claim("loginPower", "myPower")
                },
                expires: DateTime.Now.AddHours(5),
                signingCredentials: signinCredentials
            );
            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenContent);
            return new AuthResponse { token = tokenString };
        }
        
        

        //// PUT: api/Auth/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Auth/5
        //public void Delete(int id)
        //{
        //}
    }
}

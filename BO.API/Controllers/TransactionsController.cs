﻿using BO.API.Services;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lemonway.Linq2Sql;
using log4net;
using System;
using System.Linq;
using System.Web.Http;

namespace BO.API.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    public class TransactionsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(TransactionsController));
        // GET: api/Transactions
        public LoadResult Get(DataSourceLoadOptions opt)
        {
            try
            {
                var connectionString = Helper.GetConnectionString();
                var db = new MbDataContext(connectionString);
                var dataSource = db.money_transactions.Where(t => t.boLogin == "hduong@lemonway.com");
                var res = DataSourceLoader.Load(dataSource, opt);
                return res;
            }catch(Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}

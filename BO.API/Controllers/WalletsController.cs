﻿using BO.API.Services;
using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Data.ResponseModel;
using Lemonway.Linq2Sql;
using log4net;
using System;
using System.Linq;
using System.Web.Http;

namespace BO.API.Controllers
{
    //[EnableCors(origins: "*", headers: "*", methods: "*")]

    [Authorize]
    public class WalletsController : ApiController
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WalletsController));


        // GET: api/Wallets
        public LoadResult Get(DataSourceLoadOptions opt)
        {
            try
            {
                var connectionString = Helper.GetConnectionString();
                var db = new MbDataContext(connectionString);
                db.Log = new Linq2SqlLogger();

                var dataSource = db.wallets.AsQueryable();
                var res = DataSourceLoader.Load(dataSource, opt);
                return res;
            }catch(Exception e)
            {
                Log.Error(e);
                throw;
            }
        }
    }
}

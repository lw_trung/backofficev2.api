﻿using System.Web;
using System.Web.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;

namespace BO.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}

﻿using log4net;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace BO.API.Services
{
    public class JwtAuthHandler : DelegatingHandler
    {

        private static readonly ILog Log = LogManager.GetLogger(typeof(JwtAuthHandler));
        protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //var response = await base.SendAsync(request, cancellationToken);
            //return response;
            HttpResponseMessage errorResponse = null;

            try
            {
                IEnumerable<string> authHeaderValues;
                request.Headers.TryGetValues("Authorization", out authHeaderValues);


                if (authHeaderValues == null)
                {
                    // next
                    return await base.SendAsync(request, cancellationToken);
                }
                      

                var bearerToken = authHeaderValues.ElementAt(0);
                var token = bearerToken.StartsWith("Bearer ") ? bearerToken.Substring(7) : bearerToken;
                var claims = ValidateToken(token);
                var context = request.GetRequestContext();
                context.Principal = claims;
                request.SetRequestContext(context);
            }
            catch (SecurityTokenInvalidSignatureException ex)
            {
                // next with no claims registed 
                //TODO: Log something here
            }
            catch(Exception ex)
            {
                Log.Error(ex);
                errorResponse = request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            var res = errorResponse != null ? errorResponse : await base.SendAsync(request, cancellationToken);
            return res;
        }

        private ClaimsPrincipal ValidateToken(string tokenString)
        {
            var secretKey = Encoding.ASCII.GetBytes("aVeryLongSecretKey");

            SecurityToken validatedToken;
            var param = new TokenValidationParameters
            {
                ClockSkew = TimeSpan.FromMinutes(10),
                ValidIssuer = "issuer",
                ValidAudience = "LemonWay.bo",
                IssuerSigningKey = new SymmetricSecurityKey(secretKey),
            };
            var handler = new JwtSecurityTokenHandler();
            var claims = handler.ValidateToken(tokenString, param, out validatedToken);
            return claims;
        }
    }
}

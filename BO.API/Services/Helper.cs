﻿

using ToolsPack.Config;
using WebserviceClient;
using Lemonway.Linq2Sql;

namespace BO.API.Services
{
    public static class Helper{
        public static string GetConnectionString()
        {
            var url = ConfigReader.Read("boUrl", "null");
            var service = ServiceFactory.GetBackoffice(url);
            var conf =service.GetConfig(new WebserviceClient.Backoffice.GetConfigRequest {
                language = "en",
                walletIp = "1.1.1.1",
                walletUa = "ua",
                wlLogin = "hduong@lemonway.com",
                wlPass = "Ch3minDuCitr0n"
            });
            var host = conf.GetConfigResult.config.db_ip;
            var login = conf.GetConfigResult.config.db_login;
            var pwd = conf.GetConfigResult.config.db_pass;
            var db = "mb_" + conf.GetConfigResult.config.mbName;
            var connectionString = ConnectionStringBuilder.Build(host, db, login, pwd);
            return connectionString;
        }
    }

}
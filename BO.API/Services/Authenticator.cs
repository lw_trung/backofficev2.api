﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace BO.API.Services
{
    public class Authenticator
    {
        public string ConnectionString;

        public Authenticator(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public int CheckAuthority(string login, string password)
        {
            int returnstatus = -2;
            using (SqlConnection conn = new SqlConnection(ConnectionString))
            {
                using (var cmd = new SqlCommand("dbo.verify_login_pass_backoffice", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@login", login));
                    cmd.Parameters.Add(new SqlParameter("@mdp", String.IsNullOrEmpty(password) ? null : getMD5(password)));
                    cmd.Parameters.Add(new SqlParameter("@mdp_sha256", String.IsNullOrEmpty(password) ? null : getSHA256Pass(password)));
                    cmd.Parameters.Add(new SqlParameter("@is_marque_blanche", "1"));
                    cmd.Parameters.Add("@returnStatus", SqlDbType.Int, 4).Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add("@user_type", SqlDbType.Int, 4).Direction = ParameterDirection.Output;

                    conn.Open();

                    cmd.ExecuteNonQuery();
                    returnstatus = Convert.ToInt32(cmd.Parameters["@returnStatus"].Value);
                    if (returnstatus == 4 && conn.Database.StartsWith("mb_dev"))
                        returnstatus = 0;
                }
            }
            return returnstatus;
        }

        private string getMD5(string input)
        {
            if (input == null)
            {
                input = "";
            }
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();

            //return input;
        }

        private string getSHA256Pass(string input)
        {
            input = "Lem0n" + input + "w@y";
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(input);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }
    }
}
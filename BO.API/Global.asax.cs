﻿using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ToolsPack.Config;

namespace BO.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(WebApiApplication));

        protected void Application_Start()
        {
            #region Print context
            Log.Info("WebService is started");
            Log.InfoFormat(".NET Framework CLR version = {0} (4.0.30319.42000 = .NET 4.6 and 4.0.30319.x = .NET 4.0-4.5)", Environment.Version);
            Log.InfoFormat("Environment.CurrentDirectory = {0}", Environment.CurrentDirectory);
            Log.InfoFormat("Directory.CurrentDirectory = {0}", Directory.GetCurrentDirectory());

            var assemblyLocation = Assembly.GetExecutingAssembly().Location;
            Log.InfoFormat("ExecutingAssembly.Location = {0}", assemblyLocation);
            Log.InfoFormat("ExecutingAssembly.Version = {0}", FileVersionInfo.GetVersionInfo(assemblyLocation).FileVersion);

            Log.InfoFormat("Environment.UserDomainName = {0}", Environment.UserDomainName);
            Log.InfoFormat("Environment.User = {0}", Environment.UserName);
            Log.InfoFormat("RunAs User = {0}", System.Security.Principal.WindowsIdentity.GetCurrent().Name);

            //Log.InfoFormat("BoUrl = {0}", ConfigReader.Read("boUrl", "null"));
            //Log.InfoFormat("runMode = {0}", ConfigReader.Read("runMode", "null"));
            
            #endregion

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
